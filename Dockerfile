FROM ruby:latest 
RUN mkdir -p /usr/src/app 
WORKDIR /usr/src/app/ 
COPY . /usr/src/app/
RUN gem install sinatra --no-ri --no-rdoc
EXPOSE 4567

CMD ["ruby", "hw.rb"]
